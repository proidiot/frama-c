
A quick way to improve on results is to use the \emph{Loop analysis}
plug-in.

\begin{quote}
The \emph{Loop analysis} plug-in performs a mostly syntactic analysis to
estimate loop bounds in the program (using heuristics, without any
soundness guarantees) and outputs a list of options to be added to the
value analysis. Running \Eva{} again with these options should improve the
precision, although it may increase analysis time. Loop analysis' main
objective is to speed up the repetitive task of finding loop bounds and
providing them as semantic unrolling (\texttt{-slevel}) counters. The
analysis may miss some loops, and the estimated bounds may be larger or
smaller, but overall it minimizes the amount of manual work required.
\end{quote}

\emph{Loop analysis} does not depend on \Eva{}, but if it has been run, the
results may be more precise. In Monocypher, both commands below give an
equivalent result (the difference is not significative in this context):

\begin{verbatim}
frama-c -load parsed.sav -loop
frama-c -load value.sav -loop
\end{verbatim}

In both cases, Loop analysis' effect is simply to produce a text output
that should be fed into \Eva{} for a new analysis:

\begin{verbatim}
[loop] Add this to your command line:
       -eva-slevel-merge-after-loop crypto_argon2i \
       -eva-slevel-merge-after-loop crypto_blake2b_final \
       ...
\end{verbatim}

You should, by now, use a shell script or a Makefile to run the Frama-C
command line, adding all the \texttt{-eva-slevel-merge-after-loop} and
\texttt{-slevel-function} lines to your command.

Let us consider that the environment variable \texttt{LOOPFLAGS}
contains the result of Loop analysis, and \texttt{EVAFLAGS} contains the
flags mentioned previously (\texttt{-eva-no-show-progress},
\texttt{-eva-builtins-auto} and \texttt{-memexec-all}). Then the
following command will re-run \Eva{} with a more detailed (and, hopefully,
precise) set of parameters:

\begin{verbatim}
frama-c -load parsed.sav $LOOPFLAGS -eva $EVAFLAGS -save value2.sav
\end{verbatim}

Opening this file on the GUI will indicate approximately 500 warnings,
which is still substantial, but much better than before. Improvements to
Loop analysis in the next release of Frama-C will allow this number to
be reduced slightly.
