// --------------------------------------------------------------------------
// --- Main React Component rendered by './index.js'
// --------------------------------------------------------------------------

/*
   Template from $(DOME)/template/Settings.js

   This module shall export a React Component that
   will be rendered (with empty props and children)
   in the settings window of your application.

*/

import React from 'react' ;

export default (() => (
  <h1 className='dome-text-title' style={{margin: 24}}>
    Settings (none)
  </h1>
));
