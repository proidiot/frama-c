/* Generated by Frama-C */
#include "stddef.h"
#include "stdio.h"
int G = 0;
void __e_acsl_globals_init(void)
{
  static char __e_acsl_already_run = 0;
  if (! __e_acsl_already_run) {
    __e_acsl_already_run = 1;
    __e_acsl_store_block((void *)(& G),(size_t)4);
    __e_acsl_full_init((void *)(& G));
  }
  return;
}

void __e_acsl_globals_clean(void)
{
  __e_acsl_delete_block((void *)(& G));
  return;
}

int main(void)
{
  int __retres;
  __e_acsl_memory_init((int *)0,(char ***)0,(size_t)8);
  __e_acsl_globals_init();
  {
    int __gen_e_acsl_valid;
    __gen_e_acsl_valid = __e_acsl_valid((void *)(& G),sizeof(int),
                                        (void *)(& G),(void *)0);
    __e_acsl_assert(__gen_e_acsl_valid,"Assertion","main","\\valid(&G)",
                    "tests/bts/issue-eacsl-145.c",9);
  }
  /*@ assert \valid(&G); */ ;
  int a = G;
  __retres = 0;
  __e_acsl_globals_clean();
  __e_acsl_memory_clean();
  return __retres;
}


