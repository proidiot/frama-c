Thank you for submitting an issue to the Frama-C team.
We propose the following template to ease the process.
Please directly edit it inline to provide the required information.

Before submitting the issue, please confirm (by adding a X in the [ ]):

- [ ] the issue has not yet been reported on [Gitlab](https://git.frama-c.com/pub/frama-c/issues);
- [ ] the issue has not yet been reported on our old
  [BTS](https://bts.frama-c.com) (*note: the old BTS is deprecated*);
- [ ] you installed Frama-C as prescribed in the [instructions](INSTALL.md).

# Contextual information

- Frama-C installation mode: *Opam, Homebrew, package from distribution, from source, ...*
- Frama-C version: *Frama-C version* (as reported by `frama-c -version`)
- Plug-in used: *Plug-in used*
- OS name: *OS name*
- OS version: *OS version*

*Please add specific information deemed relevant with regard to this issue.*

# Steps to reproduce the issue

*Please indicate here steps to follow to get a [minimal, complete, and verifiable example](https://stackoverflow.com/help/mcve) which reproduces the issue.*

# Expected behaviour

*Please explain here what is the expected behaviour.*

# Actual behaviour

*Please explain here what is the actual (faulty) behaviour.*

# Fix ideas

*Please tell us if you already tried some work-arounds or have some ideas to solve this issue.*
