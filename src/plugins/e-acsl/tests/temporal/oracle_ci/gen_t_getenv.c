/* Generated by Frama-C */
#include "stddef.h"
#include "stdio.h"
#include "stdlib.h"
char *__gen_e_acsl_literal_string_2;
char *__gen_e_acsl_literal_string;
extern int __e_acsl_sound_verdict;

/*@ requires valid_name: valid_read_string(name);
    ensures null_or_valid_result: \result ≡ \null ∨ \valid(\result);
    assigns \result;
    assigns \result \from __fc_env[0 ..], (indirect: name), *(name + (0 ..));
 */
char *__gen_e_acsl_getenv(char const *name);

/*@ requires valid_name: valid_read_string(name);
    ensures null_or_valid_result: \result ≡ \null ∨ \valid(\result);
    assigns \result;
    assigns \result \from __fc_env[0 ..], (indirect: name), *(name + (0 ..));
 */
char *__gen_e_acsl_getenv(char const *name)
{
  char *__retres;
  __e_acsl_store_block((void *)(& __retres),(size_t)8);
  __e_acsl_temporal_reset_parameters();
  __e_acsl_temporal_reset_return();
  __retres = getenv(name);
  __e_acsl_temporal_store_nblock((void *)(& __retres),(void *)*(& __retres));
  {
    int __gen_e_acsl_or;
    __e_acsl_temporal_save_return((void *)(& __retres));
    /*@ assert
        Eva: ptr_comparison: \pointer_comparable((void *)__retres, (void *)0);
    */
    if (__retres == (char *)0) __gen_e_acsl_or = 1;
    else {
      int __gen_e_acsl_valid;
      __gen_e_acsl_valid = __e_acsl_valid((void *)__retres,sizeof(char),
                                          (void *)__retres,
                                          (void *)(& __retres));
      __gen_e_acsl_or = __gen_e_acsl_valid;
    }
    __e_acsl_assert(__gen_e_acsl_or,"Postcondition","getenv",
                    "\\result == \\null || \\valid(\\result)",
                    "FRAMAC_SHARE/libc/stdlib.h",488);
    __e_acsl_delete_block((void *)(& __retres));
    return __retres;
  }
}

void __e_acsl_globals_init(void)
{
  static char __e_acsl_already_run = 0;
  if (! __e_acsl_already_run) {
    __e_acsl_already_run = 1;
    __gen_e_acsl_literal_string_2 = "PATH";
    __e_acsl_store_block((void *)__gen_e_acsl_literal_string_2,
                         sizeof("PATH"));
    __e_acsl_full_init((void *)__gen_e_acsl_literal_string_2);
    __e_acsl_mark_readonly((void *)__gen_e_acsl_literal_string_2);
    __gen_e_acsl_literal_string = "HOME";
    __e_acsl_store_block((void *)__gen_e_acsl_literal_string,sizeof("HOME"));
    __e_acsl_full_init((void *)__gen_e_acsl_literal_string);
    __e_acsl_mark_readonly((void *)__gen_e_acsl_literal_string);
  }
  return;
}

int main(int argc, char const **argv)
{
  int __retres;
  __e_acsl_memory_init(& argc,(char ***)(& argv),(size_t)8);
  __e_acsl_globals_init();
  char *g1 = (char *)0;
  __e_acsl_temporal_store_nblock((void *)(& g1),(void *)0);
  __e_acsl_store_block((void *)(& g1),(size_t)8);
  __e_acsl_full_init((void *)(& g1));
  __e_acsl_full_init((void *)(& g1));
  __e_acsl_temporal_reset_parameters();
  __e_acsl_temporal_reset_return();
  g1 = __gen_e_acsl_getenv(__gen_e_acsl_literal_string);
  __e_acsl_temporal_pull_return((void *)(& g1));
  __e_acsl_temporal_reset_parameters();
  __e_acsl_temporal_reset_return();
  char *g2 = __gen_e_acsl_getenv(__gen_e_acsl_literal_string_2);
  __e_acsl_temporal_pull_return((void *)(& g2));
  __e_acsl_store_block((void *)(& g2),(size_t)8);
  __e_acsl_full_init((void *)(& g2));
  {
    int __gen_e_acsl_or;
    /*@ assert
        Eva: ptr_comparison: \pointer_comparable((void *)g1, (void *)0);
    */
    if (g1 == (char *)0) __gen_e_acsl_or = 1;
    else {
      int __gen_e_acsl_initialized;
      int __gen_e_acsl_and;
      __gen_e_acsl_initialized = __e_acsl_initialized((void *)(& g1),
                                                      sizeof(char *));
      if (__gen_e_acsl_initialized) {
        int __gen_e_acsl_valid;
        __gen_e_acsl_valid = __e_acsl_valid((void *)g1,sizeof(char),
                                            (void *)g1,(void *)(& g1));
        __gen_e_acsl_and = __gen_e_acsl_valid;
      }
      else __gen_e_acsl_and = 0;
      __gen_e_acsl_or = __gen_e_acsl_and;
    }
    __e_acsl_assert(__gen_e_acsl_or,"Assertion","main",
                    "g1 == \\null || \\valid(g1)",
                    "tests/temporal/t_getenv.c",14);
  }
  /*@ assert g1 ≡ \null ∨ \valid(g1); */ ;
  {
    int __gen_e_acsl_or_2;
    /*@ assert
        Eva: ptr_comparison: \pointer_comparable((void *)g2, (void *)0);
    */
    if (g2 == (char *)0) __gen_e_acsl_or_2 = 1;
    else {
      int __gen_e_acsl_initialized_2;
      int __gen_e_acsl_and_2;
      __gen_e_acsl_initialized_2 = __e_acsl_initialized((void *)(& g2),
                                                        sizeof(char *));
      if (__gen_e_acsl_initialized_2) {
        int __gen_e_acsl_valid_2;
        __gen_e_acsl_valid_2 = __e_acsl_valid((void *)g2,sizeof(char),
                                              (void *)g2,(void *)(& g2));
        __gen_e_acsl_and_2 = __gen_e_acsl_valid_2;
      }
      else __gen_e_acsl_and_2 = 0;
      __gen_e_acsl_or_2 = __gen_e_acsl_and_2;
    }
    __e_acsl_assert(__gen_e_acsl_or_2,"Assertion","main",
                    "g2 == \\null || \\valid(g2)",
                    "tests/temporal/t_getenv.c",15);
  }
  /*@ assert g2 ≡ \null ∨ \valid(g2); */ ;
  __retres = 0;
  __e_acsl_delete_block((void *)(& g2));
  __e_acsl_delete_block((void *)(& g1));
  __e_acsl_memory_clean();
  return __retres;
}


