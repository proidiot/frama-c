// --------------------------------------------------------------------------
// --- Main React Component rendered by './index.js'
// --------------------------------------------------------------------------

/*
   Template from $(DOME)/template/Application.js

   This module shall export a React Component that
   will be rendered (with empty props and children)
   in the main window of your application.

*/

import React from 'react' ;
import * as Dome from 'dome' ;

export default (() => (
  <h1 className='dome-text-title' style={{margin: 24}}>
    Hello World!
  </h1>
));
